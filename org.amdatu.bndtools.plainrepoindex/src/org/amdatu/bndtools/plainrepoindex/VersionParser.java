/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bndtools.plainrepoindex;

import static java.lang.Character.isDigit;
import static java.lang.Character.isLetterOrDigit;
import static java.lang.Character.isWhitespace;

import java.util.Arrays;

/**
 * Provides a parser for version strings that is quite lenient regarding the
 * different types of versions it can handle.
 * <p>
 * For example, it is capable of handling versions like:
 * </p>
 * <ul>
 * <li>"<tt>1.2.3</tt>": simple plain version;</li>
 * <li>"<tt>1-2_3</tt>": simple plain version with alternative separators;</li>
 * <li>"<tt>my-test-1.2.3</tt>": leading text is ignored;</li>
 * <li>"<tt>1.2.3-SNAPSHOT</tt>": qualifier = 'SNAPSHOT';</li>
 * <li>"<tt>1.2.3-beta1</tt>": qualifier = 'beta1';</li>
 * <li>"<tt>1.2.3.4</tt>": qualifier = '4';</li>
 * <li>"<tt>1.2.3c</tt>": qualifier = 'c';</li>
 * <li>"<tt>1-SNAPSHOT</tt>": minor & micro = '0', qualifier = 'SNAPSHOT';</li>
 * <li>"<tt>r1234</tt>": minor & micro = '0', qualifier = '';</li>
 * <li>"<tt>1.2.v20110101</tt>": micro = '0', qualifier = '20110101'.</li>
 * </ul>
 */
public final class VersionParser {
    /**
     * Denotes the result of a parsed version.
     */
    public static class Result {
        public final String m_prefix;
        public final String m_version;

        /**
         * Creates a new {@link Result} instance.
         * 
         * @param prefix the text before the version;
         * @param version the normalized version.
         */
        public Result(String prefix, String version) {
            m_prefix = prefix;
            m_version = version;
        }
    }

    /**
     * Parses the given input text.
     * 
     * @param input the input text to parse.
     * @return the parse result, never <code>null</code>.
     * @throws IllegalArgumentException in case the given version was <code>null</code>.
     */
    public static Result parse(String input) {
        if (input == null) {
            throw new IllegalArgumentException("Input cannot be null!");
        }

        final int length = input.length();
        String[] versionParts = { "", "", "", "" };
        int versionPartIdx = 0;
        int versionStart = -1;

        for (int i = 0; versionPartIdx < versionParts.length && i < length; i++) {
            boolean anyPartSet = !"".equals(versionParts[0]) || !"".equals(versionParts[1]) || !"".equals(versionParts[2]);

            char c = input.charAt(i);
            if (isDigit(c)) {
                if (!anyPartSet) {
                    // this is merely a hint that the version starts before the
                    // current character...
                    versionStart = i - 1;
                }
                versionParts[versionPartIdx] = versionParts[versionPartIdx] + c;
            }
            else {
                if (isWhitespace(c)) {
                    // eat the space
                }
                else if (c == '.' || c == '-' || c == '_') {
                    if (anyPartSet) {
                        versionPartIdx++;

                        int partSetCount = 0;
                        for (String part : versionParts) {
                            if (!"".equals(part)) {
                                partSetCount++;
                            }
                        }
                        if ((versionPartIdx >= versionParts.length) && (i < (length - 1)) && (partSetCount < 3)) {
                            // Found a false positive, we're probably mislead
                            // by numbers in the prefix. Reset everything and 
                            // go back to where we think the version started 
                            // and try again...
                            i = versionStart + 1;
                            versionPartIdx = 0;
                            Arrays.fill(versionParts, "");
                        }
                    }
                }
                else {
                    if (anyPartSet && (isLetterOrDigit(c) || (c == '-') || (c == '_'))) {
                        // at least one part of the version was already parsed;
                        // assume the letter denotes the start of the qualifier...
                        versionPartIdx = 3;
                        versionParts[versionPartIdx] = versionParts[versionPartIdx] + c;
                    }
                }
            }
        }

        // Strip the leading zero's...
        for (int i = 0; i < versionParts.length - 1; i++) {
            versionParts[i] = cleanup(versionParts[i]);
        }

        String prefix = "";
        if (versionStart > 0) {
            // avoid single letter prefixes...
            prefix = input.substring(0, versionStart);
            if (prefix.endsWith("-") || prefix.endsWith(".") || prefix.endsWith("_")) {
                prefix = prefix.substring(0, prefix.length() - 1);
            }
        }

        StringBuilder normalizedVersion = new StringBuilder();
        normalizedVersion.append(versionParts[0]).append(".");
        normalizedVersion.append(versionParts[1]).append(".");
        normalizedVersion.append(versionParts[2]);
        if ((versionParts[3] != null) && !"".equals(versionParts[3])) {
            normalizedVersion.append(".").append(versionParts[3]);
        }

        return new Result(prefix, normalizedVersion.toString());
    }

    /**
     * Strips leading zeros from the given input, except when the input itself only contains of a single zero.
     * In case the given input is empty, "0" is returned.
     * 
     * @param input the input to strip.
     * @return the stripped input.
     */
    private static String cleanup(String input) {
        if (input == null || "".equals(input)) {
            return "0";
        }
        if ("0".equals(input)) {
            return input;
        }
        int idx = 0;
        while ((idx < input.length()) && (input.charAt(idx) == '0')) {
            idx++;
        }
        if (idx == 0) {
            return input;
        }
        return input.substring(idx);
    }
}
