/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.amdatu.bndtools.plainrepoindex;

import java.io.File;

/**
 * Provides an abstraction of a JAR-file.
 */
public interface Resource {
    
    /**
     * @return the file representing the actual resource.
     */
    File getFile();

    /**
     * @return a "bundle symbolic name" for this resource.
     */
    String getBSN();
    
    /**
     * @return a URI for this resource.
     */
    String getURI();
    
    /**
     * @return a version for this resource.
     */
    String getVersion();
}
