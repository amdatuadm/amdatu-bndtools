/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bndtools.plainrepoindex;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Provides a minimalistic R5-OBR index generator.
 */
public class R5ObrIndexer implements Indexer {
    private static final char[] HEX =
    { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    private static final String INDEX_NAME = "index.xml";

    private final PrintWriter m_writer;

    /**
     * Creates a new {@link R5ObrIndexer} instance.
     * 
     * @param repositoryName the name of the repository to generate;
     * @param outputDirectory the output directory to write the index in.
     * @throws IOException in case of I/O problems writing into the given
     *         output directory.
     */
    public R5ObrIndexer(String repositoryName, File outputDirectory) throws IOException {
        m_writer = new PrintWriter(new File(outputDirectory, INDEX_NAME));

        writePreamble(repositoryName);
    }

    /**
     * Creates a new {@link R5ObrIndexer} instance.
     * 
     * @param repositoryName the name of the repository to generate;
     * @param os the output stream to write the index to.
     * @throws IOException in case of I/O problems writing into the given
     *         output stream.
     */
    public R5ObrIndexer(String repositoryName, OutputStream os) throws IOException {
        m_writer = new PrintWriter(os);

        writePreamble(repositoryName);
    }

    public void close() throws IOException {
        try {
            m_writer.println("</repository>");
            m_writer.flush();
        }
        finally {
            m_writer.close();
        }
    }

    public void index(Resource resource) throws Exception {
        String bsn = resource.getBSN();
        String contentHash = calculateHash(resource.getFile());
        String size = Long.toString(resource.getFile().length());
        String uri = resource.getURI();
        String version = resource.getVersion().toString();

        m_writer.append("<resource>\n")
            .append("  <capability namespace='osgi.identity'>\n")
            .append("    <attribute name='osgi.identity' value='").append(bsn).append("'/>\n")
            .append("    <attribute name='version' type='Version' value='").append(version).append("'/>\n")
            // TODO needs an additional type attribute with value = 'osgi.bundle'?
            .append("  </capability>\n")
            .append("  <capability namespace='osgi.content'>\n")
            .append("    <attribute name='osgi.content' value='").append(contentHash).append("'/>\n")
            .append("    <attribute name='url' value='").append(uri).append("'/>\n")
            .append("    <attribute name='size' value='").append(size).append("'/>\n")
            .append("    <attribute name='mime' value='application/x-jar'/>\n")
            .append("  </capability>\n")
            .append("</resource>\n");
    }

    /**
     * Calculates a SHA-256 hash from the contents of the given file.
     * <p>
     * This code is based/borrowed from the 'repoindex' project, see <a
     * href="https://github.com/njbartlett/repoindex">this project on GitHub</a>
     * (made by N.Bartlett).
     * </p>
     * 
     * @param file
     *        the file to calculate the SHA-256 hash for.
     * @return the SHA-256 hash as hex-string.
     * @throws IOException
     *         in case of I/O problems accessing the given file;
     * @throws NoSuchAlgorithmException
     *         in case the SHA-256 digest is not supported on this platform.
     */
    private String calculateHash(File file) throws IOException, NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] buf = new byte[4096];

        InputStream stream = null;
        try {
            stream = new FileInputStream(file);
            while (true) {
                int bytesRead = stream.read(buf, 0, buf.length);
                if (bytesRead < 0) {
                    break;
                }

                digest.update(buf, 0, bytesRead);
            }
        }
        finally {
            if (stream != null) {
                stream.close();
            }
        }

        return toHexString(digest.digest());
    }

    /**
     * Creates a hex-string representation of the given bytes.
     * 
     * @param data the data to return as hex-string.
     * @return a hex-string representation of the given data.
     */
    private String toHexString(byte[] data) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            sb.append(HEX[(data[i] >> 4) & 0x0F]);
            sb.append(HEX[data[i] & 0x0F]);
        }
        return sb.toString();
    }

    /**
     * Writes the preamble for the index.
     * 
     * @param repositoryName the name of the repository to use in the index.
     */
    private void writePreamble(String repositoryName) {
        m_writer.println("<?xml version='1.0' encoding='utf-8'?>");
        m_writer.printf("<repository name='%s' xmlns='http://www.osgi.org/xmlns/repository/v1.0.0'>%n", repositoryName);
    }
}
