/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bndtools.plainrepoindex;

import java.io.Closeable;

/**
 * Denotes an abstract generator for a OBR index.
 */
public interface Indexer extends Closeable {
    /**
     * Indexes a given resource.
     * 
     * @param resource the resource that is being indexed.
     * 
     * @throws Exception in case of exceptions.
     */
    void index(Resource resource) throws Exception;
}