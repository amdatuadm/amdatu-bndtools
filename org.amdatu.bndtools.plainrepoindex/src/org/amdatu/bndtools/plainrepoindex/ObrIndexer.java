/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bndtools.plainrepoindex;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * Provides a minimalistic pre-R5 OBR index generator.
 */
public class ObrIndexer implements Indexer {
    private static final String INDEX_NAME = "repository.xml";

    private final PrintWriter m_writer;

    /**
     * Creates a new {@link ObrIndexer} instance.
     * 
     * @param repositoryName the name of the repository to generate;
     * @param outputDirectory the output directory to write the index in.
     * @throws IOException in case of I/O problems writing into the given
     *         output directory.
     */
    public ObrIndexer(String repositoryName, File outputDirectory) throws IOException {
        m_writer = new PrintWriter(new File(outputDirectory, INDEX_NAME));

        writePreamble(repositoryName);
    }

    /**
     * Creates a new {@link ObrIndexer} instance.
     * 
     * @param repositoryName the name of the repository to generate;
     * @param os the output stream to write the index to.
     * @throws IOException in case of I/O problems writing into the given
     *         output stream.
     */
    public ObrIndexer(String repositoryName, OutputStream os) throws IOException {
        m_writer = new PrintWriter(os);

        writePreamble(repositoryName);
    }

    public void close() throws IOException {
        try {
            m_writer.println("</repository>");
            m_writer.flush();
        }
        finally {
            m_writer.close();
        }
    }

    public void index(Resource resource) throws Exception {
        String bsn = resource.getBSN();
        String size = Long.toString(resource.getFile().length());
        String uri = resource.getURI();
        String version = resource.getVersion().toString();

        m_writer.append("<resource id='").append(bsn).append("/").append(version)
            .append("' symbolicname='").append(bsn)
            .append("' uri='").append(uri)
            .append("' version='").append(version).append("'>\n")
            .append("  <size>").append(size).append("</size>\n")
            .append("  <capability name='bundle'>\n")
            .append("    <p n='manifestversion' v='2'/>\n")
            .append("    <p n='symbolicname' v='").append(bsn).append("'/>\n")
            .append("    <p n='version' t='version' v='").append(version).append("'/>\n")
            .append("  </capability>\n")
            .append("</resource>\n");
    }

    /**
     * Writes the preamble for the index.
     * 
     * @param repositoryName the name of the repository to use in the index.
     */
    private void writePreamble(String repositoryName) {
        m_writer.println("<?xml version='1.0' encoding='utf-8'?>");
        m_writer.println("<?xml-stylesheet type='text/xsl' href='http://www2.osgi.org/www/obr2html.xsl'?>");
        m_writer.printf("<repository name='%s'>%n", repositoryName);
    }
}
