/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bndtools.plainrepoindex.cli;

import static org.amdatu.bndtools.plainrepoindex.cli.IndexerFactory.PRE_R5_OBR;
import static org.amdatu.bndtools.plainrepoindex.cli.IndexerFactory.R5_OBR;
import static org.amdatu.bndtools.plainrepoindex.cli.IndexerFactory.create;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.amdatu.bndtools.plainrepoindex.Indexer;
import org.amdatu.bndtools.plainrepoindex.JarResource;
import org.amdatu.bndtools.plainrepoindex.Resource;

/**
 * Provides a main entry point for creating repository indexes for a certain
 * directory with plain JAR files.
 * <p>
 * 
 */
public class Main {
    private static final String DEFAULT_REPOSITORY_NAME = "Repository";

    private File m_sourceDirectory;
    private Indexer[] m_indexers;

    /**
     * MAIN ENTRY POINT.
     * 
     * @param args the command line arguments.
     */
    public static void main(String[] args) throws Exception {
        Main main = new Main();

        main.parseCommandLine(args);
        main.generateIndexes();
    }

    /**
     * Generates the indexes.
     */
    public void generateIndexes() throws Exception {
        index(m_sourceDirectory, m_indexers);
    }

    /**
     * Parses the command line arguments.
     * 
     * @param args the command line arguments.
     */
    public void parseCommandLine(String[] args) throws IOException {
        File sourceDir = new File(System.getProperty("user.dir"));
        File destDir = sourceDir;

        String repoName = DEFAULT_REPOSITORY_NAME;
        Set<String> indexerNames = new HashSet<String>();
        indexerNames.add(PRE_R5_OBR);
        indexerNames.add(R5_OBR);

        int length = args.length;
        for (int i = 0; i < length; i++) {
            String arg = args[i];
            if ("-h".equals(arg)) {
                usage();
            }
            else if ("-n".equals(arg)) {
                repoName = (i < length - 1) ? args[++i] : "";
                if (repoName.startsWith("-")) {
                    // No repository name given...
                    usage();
                }
            }
            else if ("-d".equals(arg)) {
                // destination directory...
                arg = (i < length - 1) ? args[++i] : null;
                if (arg == null || arg.startsWith("-")) {
                    // No argument given...
                    usage();
                }
                destDir = new File(arg);
            }
            else if ("-r".equals(arg)) {
                arg = (i < length - 1) ? args[++i] : "";
                if (!"*".equals(arg)) {
                    indexerNames.clear();

                    String[] params = arg.split(",");
                    for (String param : params) {
                        if (PRE_R5_OBR.equals(param) || R5_OBR.equals(param)) {
                            indexerNames.add(param);
                        }
                        else {
                            usage();
                        }
                    }
                }
            }
            else if ("-s".equals(arg)) {
                // Source directory...
                arg = (i < length - 1) ? args[++i] : null;
                if (arg == null || arg.startsWith("-")) {
                    // No argument given...
                    usage();
                }
                sourceDir = new File(arg);
            }
        }

        if (indexerNames.isEmpty()) {
            System.out.println("No indexers defined!");
            usage();
        }
        if (!sourceDir.isDirectory()) {
            System.out.println("Invalid source directory: " + sourceDir);
            usage();
        }
        if (!destDir.isDirectory()) {
            System.out.println("Invalid destination directory: " + destDir);
            usage();
        }

        m_sourceDirectory = sourceDir;
        m_indexers = new Indexer[indexerNames.size()];

        int i = 0;
        for (String name : indexerNames) {
            m_indexers[i++] = create(name, repoName, destDir);
        }
    }

    /**
     * Recursively walks across all resources in the given source directory,
     * searching for JAR files, and handing off each found JAR file to the
     * given array of indexers.
     * 
     * @param sourceDirectory the source directory to walk;
     * @param indexers the indexers to hand of each found JAR file off to.
     * @throws Exception in case of problems accessing the found JAR file.
     */
    protected void index(File sourceDirectory, Indexer[] indexers) throws Exception {
        index(sourceDirectory, sourceDirectory, indexers);

        for (Indexer indexer : indexers) {
            indexer.close();
        }
    }

    /**
     * Handles a given JAR-file by handing it off to the given array with
     * indexers.
     * 
     * @param baseDir the base directory this walk started (useful to
     *        determine relative paths);
     * @param file the file resource to handle;
     * @param indexers the indexers to hand of each found JAR file off to.
     * @throws Exception in case of problems accessing the found JAR file.
     */
    private void handleResource(File baseDir, File file, Indexer[] indexers) throws Exception {
        Resource resource = parseResource(baseDir, file);

        for (Indexer indexer : indexers) {
            indexer.index(resource);
        }
    }

    /**
     * Recursively walks across all resources in the given source directory,
     * searching for JAR files, and handing off each found JAR file to the
     * given array of indexers.
     * 
     * @param baseDir the base directory this walk started (useful to
     *        determine relative paths);
     * @param dir the current directory to walk;
     * @param indexers the indexers to hand of each found JAR file off to.
     * @throws Exception in case of problems accessing the found JAR file.
     */
    private void index(File baseDir, File dir, Indexer[] indexers) throws Exception {
        File[] files = dir.listFiles();
        if (files == null) {
            throw new IOException("Failed to list files for " + dir);
        }

        for (File file : files) {
            if (file.isFile() && file.getName().endsWith(".jar")) {
                handleResource(baseDir, file, indexers);
            }
            else if (file.isDirectory()) {
                // Recurse into the sub directory...
                index(baseDir, file, indexers);
            }
        }
    }

    /**
     * Parses the given file resource into a more abstract {@link Resource}.
     * 
     * @param baseDir the base directory this walk started (useful to
     *        determine relative paths);
     * @param file the file resource to handle;
     * @return a {@link Resource} instance, never <code>null</code>.
     */
    private Resource parseResource(File baseDir, File file) {
        return JarResource.parse(baseDir, file);
    }

    /**
     * Shows a short help text and exits.
     */
    private void usage() {
        System.out.println("Usage: Main [-d <destination dir>] [-n <repository name>] [-r (*|{obr,r5obr})] [-s <source dir>]");
        System.out.println("Where:");
        System.out.println("\t-d denotes the destination directory where the index files should be generated.");
        System.out.println("\t   Defaults to the current working directory;");
        System.out.println("\t-n denotes an optional name of the repository index to generate.");
        System.out.println("\t   Defaults to 'Repository' if omitted;");
        System.out.println("\t-r denotes which repository indexes to generate (separated by a comma).");
        System.out.println("\t   Defaults to both pre-R5 OBR and R5 OBR indexes;");
        System.out.println("\t-s denotes the source directory that should be indexed.");
        System.out.println("\t   Defaults to the current working directory.");
        System.exit(1);
    }
}
