/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.amdatu.bndtools.plainrepoindex.cli;

import java.io.File;
import java.io.IOException;

import org.amdatu.bndtools.plainrepoindex.Indexer;
import org.amdatu.bndtools.plainrepoindex.ObrIndexer;
import org.amdatu.bndtools.plainrepoindex.R5ObrIndexer;

/**
 * Provides a factory for {@link Indexer}s.
 */
public final class IndexerFactory {
    public static final String PRE_R5_OBR = "obr";
    public static final String R5_OBR = "r5obr";

    /**
     * Creates a new {@link IndexerFactory}, not used.
     */
    private IndexerFactory() {
        // Nop
    }

    /**
     * @param name
     * @param repositoryName
     * @param outputDirectory
     * @return
     * @throws IOException
     */
    public static Indexer create(String name, String repositoryName, File outputDirectory) throws IOException {
        if (PRE_R5_OBR.equals(name)) {
            return new ObrIndexer(repositoryName, outputDirectory);
        } else if (R5_OBR.equals(name)) {
            return new R5ObrIndexer(repositoryName, outputDirectory);
        } else {
            throw new IllegalArgumentException("Invalid indexer name: " + name);
        }
    }
}
