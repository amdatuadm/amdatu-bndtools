/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.amdatu.bndtools.plainrepoindex;

import java.io.File;

/**
 * Provides a JAR-file resource.
 */
public class JarResource implements Resource {
    /**
     * Default version string to use in case no version could be determined
     * from the name of a file.
     */
    private static final String DEFAULT_VERSION = "0.0.0";

    private final String m_bsn;
    private final String m_uri;
    private final String m_version;
    private final File m_file;

    /**
     * Creates a new {@link JarResource} instance.
     */
    public JarResource(File file, String uri, String bsn, String version) {
        m_file = file;
        m_uri = uri;
        m_bsn = bsn;
        m_version = version;
    }

    /**
     * Parses the given file resource into a more abstract {@link Resource}.
     * 
     * @param baseDir the base directory this walk started (useful to
     *        determine relative paths);
     * @param file the file resource to handle;
     * @return a {@link Resource} instance, never <code>null</code>.
     */
    public static Resource parse(File baseDir, File file) {
        // files should in the form of <base>-<version>.jar, where <base>
        // will be used as the BSN, and <version> as its version. If a
        // file does *not* have a dash in it, 0.0.0 will be used as
        // version instead...
        String name = file.getName();
        if (name.endsWith(".jar")) {
            // remove the file extension...
            name = name.substring(0, name.length() - 4);
        }

        String bsn;
        String uri = file.getAbsolutePath().substring(baseDir.getAbsolutePath().length() + 1);
        String version;

        int i = name.lastIndexOf('-');
        if (i >= 0) {
            // Parse out the "bsn", and version of the file...
            VersionParser.Result result = VersionParser.parse(name);
            
            bsn = result.m_prefix;
            version = result.m_version;
        }
        else {
            // no dash in file name, assume the version = 0.0.0
            bsn = name;
            version = DEFAULT_VERSION;
        }

        return new JarResource(file, uri, bsn, version);
    }
    
    /**
     * Parses the given file resource into a more abstract {@link Resource}.
     * 
     * @param baseDir the base directory this walk started (useful to
     *        determine relative paths);
     * @param fileName the <em>relative</em> file name to handle as resource;
     * @return a {@link Resource} instance, never <code>null</code>.
     */
    public static Resource parse(File baseDir, String fileName) {
        return parse(baseDir, new File(baseDir, fileName));
    }

    public String getBSN() {
        return m_bsn;
    }

    public File getFile() {
        return m_file;
    }

    public String getURI() {
        return m_uri;
    }

    public String getVersion() {
        return m_version;
    }
}