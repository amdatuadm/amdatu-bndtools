# org.amdatu.bndtools.plainrepoindex

This project provides a simple index generator for both pre-R5 as well as R5
OBRs. The difference with libraries as 'bindex' and 'bindex2' is that this
project allows a set of non-OSGi JAR files to be indexed, allowing them to be
used as if they were real OSGi bundles.

There are two artifacts that come out of this project: a "lib"-version, 
containing only the core functionality to be used/embedded by other libraries.
The "cli"-version provides a simple command-line interface for the core
functionality, allowing it to be used directly out of the box.

To generate an index for a plain JARs, the filename is used to determine both
the symbolic name and its version. This means that the filename needs to 
encode this information somehow. Fortunately, most JAR files already use such
kind of naming convention, like:

* "file-1.2.3";
* "file-1-2_3";
* "my-test-file-1.2.3";
* "file-1.2.3-SNAPSHOT";
* "file-1.2.3-beta1";
* "file-1.2.3.4";
* "file-1.2.3c";
* "file-1-SNAPSHOT";
* "file-r1234";
* "filer1234";
* file-1.2.v20110101".
 

## Usage

The resulting "cli"-JAR file can be run directly, and accepts the following
arguments:

* `-n <repository name>` to set a custom repository name for the indexes,
  defaults to 'Repository';
* `-d <dir>` sets the destination directory in which the indexes should be
  written, defaults to the current working directory;
* `-s <dir>` sets the source directory in which the files are indexed. Defaults
  to the current working directory;
* `-r (*|{obr,r5obr})` defines which types of repositories to generate. Use
  `obr` to generate a pre-R5 OBR index and `r5obr` to generate a R5-OBR index.
  Defaults to `*`, meaning that both types of indexes are to be generated.
* `-h` displays a short help text on the library.

Common usage pattern:

    $ java -jar org.amdatu.bndtools.plainrepoindex.cli.jar -r obr -n "Plain JARs repository"

## To do

It would be nice to let this library integrate with Bndtools in order to
(re)generate the index files automatically when new files are added to the
repository. For now, this is can only be done by manually running this library.

## License

Copyright (c) 2010-2012 The Amdatu Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

