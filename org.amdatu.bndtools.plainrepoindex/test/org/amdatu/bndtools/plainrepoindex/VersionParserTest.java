/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.amdatu.bndtools.plainrepoindex;

import junit.framework.TestCase;

/**
 * Test cases for {@link VersionParser}.
 */
public class VersionParserTest extends TestCase {

    public void testEmptyVersionOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("");

        assertEquals("0.0.0", result);
    }

    public void testMajorMinorMicroQualifierVersionOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("1.2.3.4");

        assertEquals("1.2.3.4", result);
    }

    public void testMajorMinorMicroSnapshotVersionOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("1.2.3-SNAPSHOT");

        assertEquals("1.2.3.SNAPSHOT", result);
    }

    public void testMajorMinorSnapshotVersionOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("1.2-SNAPSHOT");

        assertEquals("1.2.0.SNAPSHOT", result);
    }

    public void testMajorMinorVersionOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("1.2");

        assertEquals("1.2.0", result);
    }

    public void testMajorSnapshotVersionOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("1-SNAPSHOT");

        assertEquals("1.0.0.SNAPSHOT", result);
    }

    public void testMajorVersionOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("1");

        assertEquals("1.0.0", result);
    }

    public void testNullVersionFail() throws Exception {
        try {
            VersionParser.parse(null);
            fail("Expected IllegalArgumentException!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testRevisionVersionOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("r123");

        assertEquals("123.0.0", result);
    }

    public void testRevisionWithQualifierVersionOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("r123c");

        assertEquals("123.0.0.c", result);
    }

    public void testSimpleVersionOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("1.2.3");

        assertEquals("1.2.3", result);
    }

    public void testVersionIgnoreSpacesOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("1- 2 _3. foo");

        assertEquals("1.2.3.foo", result);
    }

    public void testVersionWithAlternativeSeparatorsOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("1-2_3.foo");

        assertEquals("1.2.3.foo", result);
    }

    public void testVersionWithAttachedQualifierOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("1.2.3foo");

        assertEquals("1.2.3.foo", result);
    }

    public void testVersionWithLeadingZerosOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("03.01.02");

        assertEquals("3.1.2", result);
    }

    public void testVersionWithoutMicroButWithQualifierOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("2.1.v20091210");

        assertEquals("2.1.0.v20091210", result);
    }

    public void testVersionWithPrefixAndLeadingTextOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("my-library-api-r3.2.1-SNAPSHOT");

        assertEquals("my-library-api", "3.2.1.SNAPSHOT", result);
    }

    public void testVersionWithPrefixAndQualifierOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("my-library-api-3.2.1-SNAPSHOT");

        assertEquals("my-library-api", "3.2.1.SNAPSHOT", result);
    }

    public void testVersionWithPrefixOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("org.code4u.log4j-1.2.3.4");

        assertEquals("org.code4u.log4j", "1.2.3.4", result);
    }

    public void testVersionWithPrefixOk2() throws Exception {
        VersionParser.Result result = VersionParser.parse("org.code4u.log4j-1.2.3-beta-2");

        assertEquals("org.code4u.log4j", "1.2.3.beta", result);
    }

    public void testVersionWithSeparatedQualifierOk() throws Exception {
        VersionParser.Result result = VersionParser.parse("1.2.3.foo");

        assertEquals("1.2.3.foo", result);
    }

    private void assertEquals(String expectedVersion, VersionParser.Result result) {
        assertEquals("", expectedVersion, result);
    }

    private void assertEquals(String expectedPrefix, String expectedVersion, VersionParser.Result result) {
        assertNotNull(result);
        assertEquals("Version mismatch,", expectedVersion, result.m_version);
        assertEquals("Prefix mismatch,", expectedPrefix, result.m_prefix);
    }
}
