/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bndtools.genindex.ant;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

/**
 * Provides a base class for all index-generating tasks.
 */
public abstract class BaseIndexTask extends Task {

    protected static final String OBR_INDEX_NAME = "repository.xml";
    protected static final String OBR_SUFFIX = ".zip";
    protected static final String R5OBR_INDEX_NAME = "index.xml";
    protected static final String R5OBR_SUFFIX = ".gz";

    private final List<FileSet> m_fileSets;
    private String m_repoName;
    private String m_repoPrefix;
    private boolean m_pretty;

    /**
     * Creates a new {@link BaseIndexTask} instance.
     */
    public BaseIndexTask() {
        m_fileSets = new ArrayList<FileSet>();
    }

    /**
     * Adds the given file sets to this task.
     * 
     * @param fileSet the file set to add, cannot be <code>null</code>.
     */
    public void addFileset(FileSet fileSet) {
        m_fileSets.add(fileSet);
    }

    /**
     * Executes this task by creating the requested indices for all defined file sets.
     */
    @Override
    public void execute() {
        validate();

        Iterator<FileSet> fileSetIter = getFileSets().iterator();
        while (fileSetIter.hasNext()) {
            FileSet fileSet = fileSetIter.next();

            try {
                generateIndicesFor(fileSet);
            }
            catch (Throwable exception) {
                if (exception instanceof BuildException) {
                    throw (BuildException) exception;
                }
                throw new BuildException("Failed to generate indices!", exception);
            }
        }
    }

    /**
     * Sets whether or not "pretty" indices should be generated, that could be read by humans.
     * 
     * @param pretty <code>true</code> if pretty indices should be generated, <code>false</code> otherwise.
     */
    public void setPrettyOutput(boolean pretty) {
        m_pretty = pretty;
    }

    /**
     * Sets the name that should appear as repository name in the generated index.
     * 
     * @param repoName the repository name to set.
     */
    public void setRepositoryName(String repoName) {
        m_repoName = repoName;
    }

    /**
     * Sets the prefix that should be used to generate the repository name. It will be suffixed by the name of the file set.
     * 
     * @param prefix the prefix to set.
     */
    public void setRepositoryPrefix(String prefix) {
        m_repoPrefix = prefix;
    }
    
    /**
     * Generates a pre-R5 OBR index for the given parameters.
     * 
     * @param repositoryName the name of the repository to use in the index;
     * @param repoDirectory the base directory of the repository;
     * @param resources the set with resources in the repository to generate the index for.
     * @throws Exception in case of problems generating an index.
     */
    protected abstract void generateObrIndex(String repositoryName, File repoDirectory, Set<File> resources)
        throws Exception;

    /**
     * Generates a R5-compliant OBR index for the given parameters.
     * 
     * @param repositoryName the name of the repository to use in the index;
     * @param repoDirectory the base directory of the repository;
     * @param resources the set with resources in the repository to generate the index for.
     * @throws Exception in case of problems generating an index.
     */
    protected abstract void generateR5ObrIndex(String repositoryName, File repoDirectory, Set<File> resources)
        throws Exception;

    /**
     * Returns the current file sets to generate indices for.
     * 
     * @return a list with file sets, never <code>null</code>.
     */
    protected final List<FileSet> getFileSets() {
        return m_fileSets;
    }

    /**
     * Returns the pre-R5 OBR index file name.
     * 
     * @return a pre-R5 OBR filename, never <code>null</code>.
     */
    protected final String getObrIndexName() {
    	return isPretty() ? OBR_INDEX_NAME : OBR_INDEX_NAME.concat(OBR_SUFFIX);
    }

    /**
     * Returns the R5-OBR index file name.
     * 
     * @return a R5-OBR filename, never <code>null</code>.
     */
    protected final String getR5ObrIndexName() {
    	return isPretty() ? R5OBR_INDEX_NAME : R5OBR_INDEX_NAME.concat(R5OBR_SUFFIX);
    }
    
    /**
     * Returns the name of the repository to use for the generated index.
     * 
     * @param fileSet the file set to base the repository name, if none is specified.
     * @return a repository name, never <code>null</code>.
     */
    protected final String getRepositoryName(FileSet fileSet) {
        if (m_repoName != null && !"".equals(m_repoName)) {
            return m_repoName;
        }
        String name = fileSet.getDir().getName();
        if (m_repoPrefix != null && !"".equals(m_repoPrefix)) {
            return String.format("%s %s", m_repoPrefix, name);
        }
        return name;
    }

    /**
     * Returns whether or not "pretty"-looking indices should be generated.
     * 
     * @return <code>true</code> if "pretty"-looking indiced should be generated, <code>false</code> otherwise.
     */
    protected boolean isPretty() {
        return m_pretty;
    }

    /**
     * Validates whether all mandatory settings are set.
     * 
     * @throws BuildException in case validation failed.
     */
    protected void validate() throws BuildException {
        if (getFileSets().isEmpty()) {
            throw new BuildException("No file set defined!");
        }
        
        Iterator<FileSet> fileSetIter = getFileSets().iterator();
        while (fileSetIter.hasNext()) {
            FileSet fs = fileSetIter.next();
            
            String[] includedFiles = fs.getDirectoryScanner().getIncludedFiles();
            if (includedFiles.length == 0) {
                throw new BuildException("Empty file set defined!");
            }
        }
    }

    /**
     * Generates the indices for the given file set.
     * 
     * @param fileSet the file set to generate the indices for, cannot be <code>null</code>.
     */
    private void generateIndicesFor(FileSet fileSet) throws Exception {
        File repoDir = fileSet.getDir();
        DirectoryScanner dirScanner = fileSet.getDirectoryScanner();

        Set<File> jarFiles = new HashSet<File>();
        String[] includedFiles = dirScanner.getIncludedFiles();
        for (int i = 0; i < includedFiles.length; i++) {
            jarFiles.add(new File(repoDir, includedFiles[i]));
        }

        String repositoryName = getRepositoryName(fileSet);

        generateR5ObrIndex(repositoryName, repoDir, jarFiles);
        generateObrIndex(repositoryName, repoDir, jarFiles);
    }

    /**
     * Provides a custom log-bridge to bridge the OSGi log-service to Ant.
     */
    @SuppressWarnings("rawtypes")
    protected final class LogBridge implements LogService {

        public void log(int level, String message) {
            BaseIndexTask.this.log(message, convertLevel(level));
        }

        public void log(int level, String message, Throwable exception) {
            BaseIndexTask.this.log(message, exception, convertLevel(level));
        }

        public void log(ServiceReference sr, int level, String message) {
            log(level, message);
        }

        public void log(ServiceReference sr, int level, String message, Throwable exception) {
            log(level, message, exception);
        }

        private int convertLevel(int level) {
            switch (level) {
                case LogService.LOG_ERROR:
                    return Project.MSG_ERR;
                case LogService.LOG_INFO:
                    return Project.MSG_INFO;
                case LogService.LOG_WARNING:
                    return Project.MSG_WARN;
            }
            return Project.MSG_DEBUG;
        }
    }
}