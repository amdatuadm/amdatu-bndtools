package org.amdatu.bndtools.genindex.ant;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.jar.JarInputStream;
import java.util.jar.Manifest;

import org.apache.tools.ant.util.FileNameMapper;

/**
 * Filename mapper that maps bundles with arbitrary names to names that
 * can be used in a bundle repository: bsn/bsn-version.jar. The "from"
 * parameter of the mapper must be set to the directory where the bundles
 * can now be found so the mapper can open them to inspect their manifest.
 */
public class BundleMapper implements FileNameMapper {
	private String m_repositoryDir;

	@Override
	public String[] mapFileName(String name) {
		if (name.toLowerCase().endsWith(".jar")) {
			File f = new File(m_repositoryDir, name);
			try (JarInputStream jis = new JarInputStream(new FileInputStream(f))){
				Manifest m = jis.getManifest();
				String bsn = (String) m.getMainAttributes().getValue("Bundle-SymbolicName");
				String ver = (String) m.getMainAttributes().getValue("Bundle-Version");
				return new String[] { bsn + "/" + bsn + "-" + ver + ".jar" };
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public void setFrom(String from) {
		m_repositoryDir = from;
	}

	@Override
	public void setTo(String to) {
	}
}
