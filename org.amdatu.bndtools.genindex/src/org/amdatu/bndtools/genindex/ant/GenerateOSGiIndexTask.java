/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.amdatu.bndtools.genindex.ant;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.osgi.impl.bundle.bindex.BundleIndexerImpl;
import org.osgi.service.bindex.BundleIndexer;
import org.osgi.service.indexer.ResourceIndexer;
import org.osgi.service.indexer.impl.RepoIndex;

/**
 * Ant-specific task to generate OBR indexes for a particular location.
 */
public class GenerateOSGiIndexTask extends BaseIndexTask {

    /**
     * Generates the index for pre-R5 OBRs.
     * 
     * @param repositoryName the name of the repository;
     * @param repoDirectory the directory where the repository resides;
     * @param resources the set of files to generate the repository for.
     */
    protected void generateObrIndex(String repositoryName, File repoDirectory, Set<File> resources) throws Exception {
        Map<String, String> config = new HashMap<String, String>();
        config.put(BundleIndexer.REPOSITORY_NAME, repositoryName);
        config.put(BundleIndexer.ROOT_URL, repoDirectory.toURI().toString());

        String indexFileName = getObrIndexName();
        if (!isPretty()) {
            // see last few lines for rationale...
            indexFileName = indexFileName.concat(".tmp");
        }

        BundleIndexerImpl indexer = new BundleIndexerImpl();

        File indexFile = new File(repoDirectory, indexFileName);
        OutputStream output = new FileOutputStream(indexFile);

        try {
            indexer.index(resources, output, config);
        }
        finally {
            try {
                output.flush();
            }
            finally {
                output.close();
            }
        }

        if (!isPretty()) {
            // Hack: the bindex tool only does compression from its CLI,
            // not its core. Hence, we need to do this ourselves...
            indexFile = compressObrIndex(repositoryName, indexFile, new File(repoDirectory, getObrIndexName()));
        }

        log("OBR Index generated to: " + indexFile);
    }

    /**
     * Generates the index for R5 OBRs.
     * 
     * @param repositoryName the name of the repository;
     * @param repoDirectory the directory where the repository resides;
     * @param resources the set of files to generate the repository for.
     */
    protected void generateR5ObrIndex(String repositoryName, File repoDirectory, Set<File> resources) throws Exception {
        Map<String, String> config = new HashMap<String, String>();
        config.put(ResourceIndexer.REPOSITORY_NAME, repositoryName);
        config.put(ResourceIndexer.ROOT_URL, repoDirectory.toURI().toString());
        if (isPretty()) {
            // Only the presence of this key will cause the output to be pretty, 
            // ie: the value does not matter...
            config.put(ResourceIndexer.PRETTY, Boolean.toString(isPretty()));
        }

        // don't use the default constructor, as it will install a console log service...
        RepoIndex indexer = new RepoIndex(new LogBridge());
        File indexFile = new File(repoDirectory, getR5ObrIndexName());

        OutputStream output = new FileOutputStream(indexFile);
        try {
            indexer.index(resources, output, config);
        }
        finally {
            try {
                output.flush();
            }
            finally {
                output.close();
            }
        }

        log("R5-OBR Index generated to: " + indexFile);
    }

    /**
     * @param repositoryName
     * @param tempIndexFile
     * @param zippedIndexFile
     * @throws IOException
     */
    private File compressObrIndex(String repositoryName, File tempIndexFile, File zippedIndexFile) throws IOException {
        try {
            CompressUtil.compressWithZIP(repositoryName, tempIndexFile, zippedIndexFile);
            return zippedIndexFile;
        } finally {
            tempIndexFile.delete();
        }
    }
}
