/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bndtools.genindex.ant;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Set;

import org.amdatu.bndtools.plainrepoindex.JarResource;
import org.amdatu.bndtools.plainrepoindex.ObrIndexer;
import org.amdatu.bndtools.plainrepoindex.R5ObrIndexer;

/**
 * Provides a index generator for plain JARs.
 */
public class GeneratePlainIndexTask extends BaseIndexTask {

    @Override
    protected void generateObrIndex(String repositoryName, File repoDirectory, Set<File> resources) throws Exception {
        File indexFile = new File(repoDirectory, getObrIndexName());

        ObrIndexer indexer = null;
        OutputStream output = new FileOutputStream(indexFile);

        try {
            indexer = new ObrIndexer(repositoryName, output);

            Iterator<File> resourceIter = resources.iterator();
            while (resourceIter.hasNext()) {
                File resource = resourceIter.next();
                indexer.index(JarResource.parse(repoDirectory, resource));
            }
        }
        finally {
            if (indexer != null) {
                indexer.close();
            }
            try {
                output.flush();
            }
            finally {
                output.close();
            }
        }

        log("Plain OBR Index generated in " + indexFile);
    }

    @Override
    protected void generateR5ObrIndex(String repositoryName, File repoDirectory, Set<File> resources) throws Exception {
        File indexFile = new File(repoDirectory, getR5ObrIndexName());

        R5ObrIndexer indexer = null;
        OutputStream output = new FileOutputStream(indexFile);

        try {
            indexer = new R5ObrIndexer(repositoryName, output);

            Iterator<File> resourceIter = resources.iterator();
            while (resourceIter.hasNext()) {
                File resource = resourceIter.next();
                indexer.index(JarResource.parse(repoDirectory, resource));
            }
        }
        finally {
            if (indexer != null) {
                indexer.close();
            }
            try {
                output.flush();
            }
            finally {
                output.close();
            }
        }

        log("Plain R5-OBR Index generated in " + indexFile);
    }
    
    @Override
    protected boolean isPretty() {
        // This method always yields "pretty"-looking indices...
        return true;
    }
}
