/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.amdatu.bndtools.genindex.ant;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Provides some utility methods to compress files in either GZip or ZIP format.
 */
class CompressUtil {

    /**
     * Creates a new {@link CompressUtil} instance, not used.
     */
    private CompressUtil() {
        // Nop
    }

    /**
     * Compresses the given input file, and writes the output to the given output file.
     * 
     * @param entryName the name of the ZIP-entry;
     * @param input the input file to compress;
     * @param output the output file to write the compressed results to.
     * @throws IOException in case of I/O problems during the compression.
     */
    public static void compressWithZIP(String entryName, File input, File output) throws IOException {
        FileOutputStream fos = new FileOutputStream(output);
        ZipOutputStream zip = new ZipOutputStream(fos);

        CRC32 checksum = determineCRC(input);

        ZipEntry ze = new ZipEntry(entryName);
        ze.setSize(input.length());
        ze.setCrc(checksum.getValue());
        zip.putNextEntry(ze);

        copyStream(input, zip);

        zip.closeEntry();
        zip.close();
    }

    /**
     * @param tempIndexFile
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    private static CRC32 determineCRC(File tempIndexFile) throws FileNotFoundException, IOException {
        CRC32 checksum = new CRC32();

        FileInputStream fis = new FileInputStream(tempIndexFile);
        try {
            byte[] buffer = new byte[4096];
            int read;
            while ((read = fis.read(buffer)) > 0) {
                checksum.update(buffer, 0, read);
            }
        }
        finally {
            fis.close();
        }
        return checksum;
    }

    /**
     * @param tempIndexFile
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    private static void copyStream(File tempIndexFile, ZipOutputStream os) throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream(tempIndexFile);
        try {
            byte[] buffer = new byte[4096];
            int read;
            while ((read = fis.read(buffer)) > 0) {
                os.write(buffer, 0, read);
            }
        }
        finally {
            fis.close();
        }
    }
}
