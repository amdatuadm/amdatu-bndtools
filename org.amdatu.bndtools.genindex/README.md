# org.amdatu.bndtools.genindex

This project provides Ant tasks for generating pre-R5 and R5-compliant OBR
index files for both OSGi as non-OSGi files.  

## Usage

To use the Ant tasks, you need to include the following snippet into your build
file in order to make them available to Ant:

    <taskdef resource="org/amdatu/bndtools/genindex/ant/taskdef.properties">
        <classpath>
            <path location="/path/to/org.amdatu.bndtools.genindex.ant.jar" />
       	</classpath>
    </taskdef>

Once this is done, you have two new Ant tasks: `genindex-plain` to generate OBR
indices for plain JARs and `genindex-osgi` to generate OBR indices for OSGi 
bundles. The tasks accept one or more `fileSet`s to denote what files are to be
included in the indexation process. 

Both tasks accept the following arguments (as attributes):

* `prettyOutput`; either "`true`" or "`false`", denotes whether or not a pretty 
  version of the index is generated (indented). Currently only works properly 
  for the `genindex-osgi` task. For this task, using `true` for this setting 
  also causes the indices to be compressed. Defaults to `false`;
* `repositoryName`; defines the name of the repository as it will appear in the
  generated index;
* `repositoryPrefix`; defines the prefix to use for the name of the repository.

Both the `repositoryName` and `repositoryPrefix` can be used to influence the 
name that appears in the generated index. If `repositoryName` is defined, it 
will always override the `repositoryPrefix` property. The `repositoryPrefix`
will be suffixed with the name of the directory of the fileSet that corresponds
to the repository. If both are omitted, `Repository` will be used as default.

For example:

    <target name="generate-plain-indices">
        <genindex-plain repositoryPrefix="Amdatu">
            <fileset dir="${projectdir}/generated" includes="**/*.jar" />
        </genindex-plain>
    </target>

In this example, we will generate indices for all JAR files in the 
`${projectdir}/generated` directory. The name of the indices will be `Amdatu 
generated`.

Another example:

	<target name="generate-osgi-indices">
		<genindex-osgi prettyOutput="false" repositoryName="Amdatu Snapshots">
			<fileset dir="/path/to/snapshots" includes="**/*.jar" />
		</genindex-osgi>
	</target>

This will cause all bundles found under `/path/to/snapshots` to be indexed to
compressed index files. The name of the index is set to `Amdatu Snapshots`.

The jar file also contains a bundle repository specific "mapper" for Ant. You
can use it in any task that supports mappers, for example the copy task:

  <copy todir="release">
    <fileset dir="repository" includes="**" />
    <mapper classname="org.amdatu.bndtools.genindex.ant.BundleMapper"
            classpath="org.amdatu.bndtools.genindex.ant.jar"
            from="repository" />
  </copy>

This will copy any bundles in the repository directory tree to a directory
called release, using the BSN/BSN-version.jar convention to name the bundle.

## License

Copyright (c) 2010-2014 The Amdatu Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

