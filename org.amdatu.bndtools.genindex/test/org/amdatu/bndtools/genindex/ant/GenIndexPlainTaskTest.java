/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bndtools.genindex.ant;


import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.types.FileSet;

/**
 * Test cases for {@link GeneratePlainIndexTask}.
 */
public class GenIndexPlainTaskTest extends AbstractGenIndexTask {

    /**
     * Creates a new {@link GenIndexPlainTaskTest} instance.
     */
    public GenIndexPlainTaskTest(String name) {
        super(name);
    }

    public void testGeneratePlainIndicesWithoutFileSetFromBuildFileFail() throws Exception {
        expectBuildExceptionContaining("genplain-without-fileset", "no fileset is defined", "No file set defined!");
    }

    public void testGeneratePlainIndicesWithFileSetFromBuildFileFail() throws Exception {
        executeTarget("genplain-with-fileset");
    }

    public void testGenIndexWithFileSetWithExclusionsOk() throws Exception {
        FileSet fileSet = createFileSet("bundle-1.0.0.jar", "sources-2.0.0.jar");
        fileSet.setExcludes("**/sources-*.jar");

        GeneratePlainIndexTask task = new GeneratePlainIndexTask();
        task.addFileset(fileSet);

        task.execute();

        assertPrettyIndicesExist(fileSet);
    }

    public void testGenIndexWithFileSetWithNonOsgiBundleOk() throws Exception {
        FileSet fileSet = createFileSet("sources-1.0.0.jar");

        GeneratePlainIndexTask task = new GeneratePlainIndexTask();
        task.setPrettyOutput(true);
        task.addFileset(fileSet);

        task.execute();

        // The task should generate the indices normally...
        assertPrettyIndicesExist(fileSet);
    }

    public void testGenIndexWithFileSetWithoutPrettyOk() throws Exception {
        FileSet fileSet = createFileSet("jar1-1.0.0.jar");

        GeneratePlainIndexTask task = new GeneratePlainIndexTask();
        task.addFileset(fileSet);

        task.execute();

        assertPrettyIndicesExist(fileSet);
    }

    public void testGenIndexWithFileSetWithPrettyOk() throws Exception {
        FileSet fileSet = createFileSet("jar1-1.0.0.jar");

        GeneratePlainIndexTask task = new GeneratePlainIndexTask();
        task.addFileset(fileSet);
        task.setPrettyOutput(true);

        task.execute();

        assertPrettyIndicesExist(fileSet);
    }

    public void testGenIndexWithoutFileSetFail() throws Exception {
        GeneratePlainIndexTask task = new GeneratePlainIndexTask();
        try {
            task.execute();

            fail("Expected BuildException!");
        }
        catch (BuildException expected) {
            // Ok; expected
        }
    }
}
