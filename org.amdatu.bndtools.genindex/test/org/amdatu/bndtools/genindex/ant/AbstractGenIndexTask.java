/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bndtools.genindex.ant;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes.Name;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

import org.apache.tools.ant.BuildFileTest;
import org.apache.tools.ant.types.FileSet;

/**
 * Provides an abstract base class for test cases involving Ant-tasks.
 */
abstract class AbstractGenIndexTask extends BuildFileTest {

    private final List<File> m_dirs = new ArrayList<File>();

    /**
     * Creates a new {@link AbstractGenIndexTask} instance.
     */
    public AbstractGenIndexTask() {
        super();
    }

    /**
     * Creates a new {@link AbstractGenIndexTask} instance.
     * 
     * @param name the name of this test case.
     */
    public AbstractGenIndexTask(String name) {
        super(name);
    }

    protected final void assertCompressedIndicesExist(FileSet fileSet) {
        File dir = fileSet.getDir();

        File obrRepo = new File(dir, "repository.xml.zip");
        assertTrue("repository.xml.zip missing!", obrRepo.exists());

        File r5obrRepo = new File(dir, "index.xml.gz");
        assertTrue("index.xml.gz missing!", r5obrRepo.exists());
    }

    protected final void assertPrettyIndicesExist(FileSet fileSet) {
        File dir = fileSet.getDir();

        File obrRepo = new File(dir, "repository.xml");
        assertTrue("repository.xml missing!", obrRepo.exists());

        File r5obrRepo = new File(dir, "index.xml");
        assertTrue("index.xml missing!", r5obrRepo.exists());
    }

    protected final void createBundleFile(File baseDir, String name) throws IOException {
        Manifest m = new Manifest();
        m.getMainAttributes().put(new Name("Bundle-SymbolicName"), "org.amdatu.bndtools.test." + name);
        createJarFile(baseDir, name, m);
    }

    protected final FileSet createFileSet(String... contentNames) throws IOException {
        File baseDir = File.createTempFile("genindex-", "");
        baseDir.delete();
        assertTrue("Failed to create temporary directory!", baseDir.mkdirs());
        m_dirs.add(baseDir);

        for (String contentName : contentNames) {
            if (contentName.startsWith("bundle")) {
                createBundleFile(baseDir, contentName);
            }
            else {
                createPlainJarFile(baseDir, contentName);
            }
        }

        FileSet fs = new FileSet();
        fs.setProject(getProject());
        fs.setDir(baseDir);
        fs.setIncludes("**/*.jar");
        return fs;
    }

    protected final void createPlainJarFile(File baseDir, String name) throws IOException {
        Manifest m = new Manifest();
        createJarFile(baseDir, name, m);
    }

    @Override
    protected void setUp() {
        configureProject("test/org/amdatu/bndtools/genindex/ant/test-build.xml");
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        for (File dir : m_dirs) {
            deltree(dir);
        }
    }

    private void createJarFile(File baseDir, String name, Manifest manifest) throws IOException {
        manifest.getMainAttributes().put(Name.MANIFEST_VERSION, "1.0");

        File outFile = new File(baseDir, name);

        FileOutputStream fos = new FileOutputStream(outFile);
        JarOutputStream jos = new JarOutputStream(fos, manifest);

        jos.close();
        fos.close();
    }

    private void deltree(File baseDir) throws IOException {
        if (baseDir.exists()) {
            File[] files = baseDir.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    deltree(file);
                }
                file.delete();
            }
        }
    }

}