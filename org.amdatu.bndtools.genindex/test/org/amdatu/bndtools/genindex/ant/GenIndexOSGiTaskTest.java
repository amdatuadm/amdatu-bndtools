/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.amdatu.bndtools.genindex.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.types.FileSet;

/**
 * Test cases for {@link GenerateOSGiIndexTask}.
 */
public class GenIndexOSGiTaskTest extends AbstractGenIndexTask {

    /**
     * Creates a new {@link GenIndexOSGiTaskTest} instance.
     */
    public GenIndexOSGiTaskTest(String name) {
        super(name);
    }

    public void testReferenceLibraryFromBuildFileOk() throws Exception {
        // Tests that we can reference the tasks from a build file...
        executeTarget("init");
        
        expectLogContaining("init", "Enter project ");
    }

    public void testGeneratePlainIndicesWithEmptyFileSetFromBuildFileFail() throws Exception {
        // Tests that the validation works...
        expectBuildExceptionContaining("genosgi-with-empty-fileset", "empty fileset defined", "Empty file set defined!");
    }

    public void testGeneratePlainIndicesWithNonEmptyFileSetFromBuildFileOk() throws Exception {
        // Tests that the validation works...
        executeTarget("genosgi-with-fileset");
    }

    public void testGenIndexWithFileSetWithExclusionsOk() throws Exception {
        FileSet fileSet = createFileSet("bundle1.jar", "sources.jar");
        fileSet.setExcludes("**/sources.jar");

        GenerateOSGiIndexTask task = new GenerateOSGiIndexTask();
        task.addFileset(fileSet);

        task.execute();

        assertCompressedIndicesExist(fileSet);
    }

    public void testGenIndexWithFileSetWithNonOsgiBundleOk() throws Exception {
        FileSet fileSet = createFileSet("sources.jar");

        GenerateOSGiIndexTask task = new GenerateOSGiIndexTask();
        task.setPrettyOutput(true);
        task.addFileset(fileSet);

        task.execute();

        // The task should generate the indices normally...
        assertPrettyIndicesExist(fileSet);
    }

    public void testGenIndexWithFileSetWithoutPrettyOk() throws Exception {
        FileSet fileSet = createFileSet("bundle1.jar");

        GenerateOSGiIndexTask task = new GenerateOSGiIndexTask();
        task.addFileset(fileSet);

        task.execute();

        assertCompressedIndicesExist(fileSet);
    }

    public void testGenIndexWithFileSetWithPrettyOk() throws Exception {
        FileSet fileSet = createFileSet("bundle1.jar");

        GenerateOSGiIndexTask task = new GenerateOSGiIndexTask();
        task.addFileset(fileSet);
        task.setPrettyOutput(true);

        task.execute();

        assertPrettyIndicesExist(fileSet);
    }

    public void testGenIndexWithoutFileSetFail() throws Exception {
        GenerateOSGiIndexTask task = new GenerateOSGiIndexTask();
        try {
            task.execute();

            fail("Expected BuildException!");
        }
        catch (BuildException expected) {
            // Ok; expected
        }
    }
}
