package org.amdatu.bndtools.testconfig;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

public class ConfigurationStepContainer implements ConfigurationStep {
	private List<ConfigurationStep> steps = new ArrayList<>();
	
	public ConfigurationStepContainer() {
		
	}
	
	public void add(ConfigurationStep step) {
		steps.add(step);
	}
	
	public void apply(TestCase testCase) {
		for (ConfigurationStep step : steps) {
			step.apply(testCase);
		}
	}
}
