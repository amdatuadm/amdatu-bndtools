package org.amdatu.bndtools.testconfig;

import junit.framework.TestCase;

public interface ConfigurationStep {
	public void apply(TestCase testCase);
}
