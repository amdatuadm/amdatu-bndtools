/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bndtools.testconfig;

import junit.framework.TestCase;

import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Utilities for helping out with common OSGi integration test operations such as
 * configuring and retrieving service instances.
 * 
 * Usage:
 * 
 * <ul>
 * <li> Make sure your test case inherits from JUnit's <code>TestCase</code> class.</li>
 * <li> Import the public methods defined in this class statically to your test case </li>
 * <li> Define a private volatile field for each service you want to inject to your test </li>
 * <li> Override the setUp() method, and do the necessary configurations. </li>
 * </ul>
 * Here is an example of configuring a service called MyService that uses the amdatu mongodb service:
 *   
 * </ul>  
 *  <pre>
 *  <code>
 *  import static org.amdatu.bndtools.testconfig.OSGiTestConfigurator.*;
 *  // other necessary imports 
 *  
 *  public class MyTestCase extends TestCase {
 *  	private volatile MyService myService;
 *  
 *  	public void setUp() {
 *  		configureTest(this,
 *  			configureFactory("org.amdatu.mongo").set("dbName", "test"),
 *  			inject(MyService.class));				
 *  	}
 *  
 *  	public void testSomething() {
 *  		assertNotNull(myService);
 *  	} 
 *  }
 *  </code>
 *  </pre>
 */
public class OSGiTestConfigurator {
	
	private static BundleContext context = FrameworkUtil.getBundle(OSGiTestConfigurator.class).getBundleContext();
	
	/**
	 * Sets up a JUnit test case, performing all specified configurations and registering
	 * all necessary services.
	 * 
	 * @param testCase The test case to set up.
	 * @param steps List of configuration steps.
	 */
	public static void configureTest(TestCase testCase, ConfigurationStep ... steps) {
		for (ConfigurationStep step : steps) {
			step.apply(testCase);
		}
	}
	
	/**
	 * Configures the specified service factory.
	 * 
	 * @param pid The pid of the service factory to configure
	 * @return ServiceConfigurator instance used to set configuration properties.
	 */
	public static ServiceFactoryConfigurator configureFactory(String pid) {
		return new ServiceFactoryConfigurator(pid);
	}
	
	/**
	 * Configures a service with the specified pid. 
	 * 
	 * @param pid The pid of the service to configure.
	 * @return ServiceConfigurator instance used to set configuration properties.
	 */
	public static ServiceConfigurator configure(String pid) {
		return new ServiceConfigurator(pid);
	}
	
	/**
	 * Injects the specified list of services to the test case.
	 * 
	 * @param serviceClass
	 * @return ConfigurationStepContainer used internally to perform the actual configurations.
	 */
	public static ConfigurationStepContainer inject(Class<?> ... serviceClassList) {
		ConfigurationStepContainer steps = new ConfigurationStepContainer();
		for (Class<?> serviceClass : serviceClassList) {
			steps.add(new ServiceInjector(serviceClass, null));
		}
		return steps;
	}
	
	/**
	 * Injects a service to the test case that matches the a specific filter string.
	 * 
	 * @param serviceClass The class of the service to inject.
	 * @param filterString The filter string the service should match.
	 * 
	 * @return ServiceInjector used internally to perform the actual configurations.
	 */
	public static ServiceInjector inject(Class<?> serviceClass, String filterString) {
		
		return new ServiceInjector(serviceClass, filterString);
	}

	static <T> T getService(Class<T> serviceClass) {
		try {
			return getService(serviceClass, null);
		} catch (InvalidSyntaxException e) {
			throw new IllegalStateException(serviceClass + " service is not available");
		}
	}
	
	@SuppressWarnings({ "unchecked" })
	private static <T> T getService(Class<T> serviceClass, String filterString)
			throws InvalidSyntaxException {
		T serviceInstance = null;

		ServiceTracker serviceTracker;
		BundleContext context = getBundleContext();
		
		if (filterString == null) {
			serviceTracker = new ServiceTracker(context,
					serviceClass.getName(), null);
		} else {
			String classFilter = "(" + Constants.OBJECTCLASS + "="
					+ serviceClass.getName() + ")";
			filterString = "(&" + classFilter + filterString + ")";
			serviceTracker = new ServiceTracker(context,
					context.createFilter(filterString), null);
		}
		
		serviceTracker.open();
		
		try {
			serviceInstance = (T) serviceTracker.waitForService(2 * 1000);

			if (serviceInstance == null) {
				throw new IllegalStateException(serviceClass + " service is not available");
			} 
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new IllegalStateException(serviceClass + " service is not available");
		}

		return serviceInstance;
	}
	
	static BundleContext getBundleContext() {
		return context;
	}
}
