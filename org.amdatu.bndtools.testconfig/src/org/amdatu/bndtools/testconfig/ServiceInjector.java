package org.amdatu.bndtools.testconfig;

import java.lang.reflect.Field;

import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.util.tracker.ServiceTracker;

import junit.framework.TestCase;
import static org.amdatu.bndtools.testconfig.OSGiTestConfigurator.*;

/**
 * Configuration step that injects a single service to a test case.
 */
public class ServiceInjector implements ConfigurationStep {
	
	private Class<?> serviceClass;
	private String filterString;
	
	public ServiceInjector(Class<?> serviceClass, String filterString) {
		this.serviceClass = serviceClass;
		this.filterString = filterString;
	}
	
	@Override
	public void apply(TestCase testCase) {
		ServiceTracker serviceTracker;
		BundleContext context = getBundleContext();
		
		if (filterString == null) {
			serviceTracker = new ServiceTracker(context,
					serviceClass.getName(), null);
		} else {
			String classFilter = "(" + Constants.OBJECTCLASS + "="
					+ serviceClass.getName() + ")";
			filterString = "(&" + classFilter + filterString + ")";
			
			try {
				serviceTracker = new ServiceTracker(context,
						context.createFilter(filterString), null);
			} catch (InvalidSyntaxException ex) {
				throw new IllegalArgumentException("Invalid filter syntax was specified for service " + serviceClass, ex);
			}
		}
		
		serviceTracker.open();
		
		try {
			Object serviceInstance = serviceTracker.waitForService(2 * 1000);

			if (serviceInstance == null) {
				throw new IllegalStateException(serviceClass + " service is not available");
			} 
			
			for (Field field : testCase.getClass().getDeclaredFields()) {
				if (field.getType().equals(serviceClass)) {
					field.setAccessible(true);
					field.set(testCase, serviceInstance);
				} 
			}
			
		} catch (InterruptedException ex) {
			throw new IllegalStateException(serviceClass + " service is not available", ex);
		} catch (IllegalArgumentException ex) {
			throw new InjectionFailedException("Failed to inject service " + serviceClass, ex);
		} catch (IllegalAccessException ex) {
			throw new InjectionFailedException("Failed to inject service " + serviceClass, ex);
		}
	}
}
