/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bndtools.testconfig;

import java.io.IOException;
import java.util.Properties;

import junit.framework.TestCase;

import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

import static org.amdatu.bndtools.testconfig.OSGiTestConfigurator.*;

/**
 * Configuration step that configures a single OSGi service factory.
 */
public class ServiceConfigurator implements ConfigurationStep {
	
	Properties properties;
	private String servicePid;
	
	public ServiceConfigurator(String servicePid) {
		this.servicePid = servicePid;
		this.properties = new Properties();
	}
	
	public ServiceConfigurator set(String key, String value) {
		properties.put(key, value);
		return this;
	}
	
	public void apply(TestCase testCase) {
		ConfigurationAdmin configurationAdmin = getService(ConfigurationAdmin.class);
		try {
			Configuration configuration = configurationAdmin.getConfiguration(servicePid, null);
			configuration.update(properties);
		} catch (IOException ex) {
			throw new RuntimeException("Exception while configuring factory", ex);
		}
	}
	
}
