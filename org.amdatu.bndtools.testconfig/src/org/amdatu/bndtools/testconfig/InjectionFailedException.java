package org.amdatu.bndtools.testconfig;

/**
 * Exception thrown when a injection failed for some reason.
 */
public class InjectionFailedException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public InjectionFailedException(String message, Throwable cause) {
		super(message, cause);
	}
}
